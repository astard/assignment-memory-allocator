#include "tests.h"

#define MALLOC_SIZE_TEST1_1 350
#define MALLOC_SIZE_TEST1_2 600
#define MALLOC_SIZE_TEST1_3 900

void test1(void* heap){
    debug("Запущен тест 1 - инициирование кучи\n");

    debug_heap(stdout, heap);

    debug("Аллоцируем тестовый блок 1\n");
    void* block1 = _malloc(MALLOC_SIZE_TEST1_1);
    if (!block1) err("Не удалось аллоцировать тестовый блок 1");
    debug_heap(stdout, heap);

    debug("Аллоцируем тестовый блок 2\n");
    void* block2 = _malloc(MALLOC_SIZE_TEST1_2);
    if (!block2) err("Не удалось аллоцировать тестовый блок 2");
    debug_heap(stdout, heap);

    debug("Аллоцируем тестовый блок 3\n");
    void* block3 = _malloc(MALLOC_SIZE_TEST1_3);
    if (!block3) err("Не удалось аллоцировать тестовый блок 3");
    debug_heap(stdout, heap);

    debug("Освобождение 1 выделенного блока\n");
    _free(block1);
    debug_heap(stdout, heap);
    
    debug("Освобождение 3 выделенного блока\n");
    _free(block3);
    debug_heap(stdout, heap);

    debug("Освобождение 2 выделенного блока\n");
    _free(block2);
    debug_heap(stdout, heap);

    debug("-------------------\n");
}