#include "tests.h"

#define MALLOC_SIZE_TEST2_1 13000

void test2(void* heap){
    debug("Запущен тест 2 - инициирование кучи\n");

    debug_heap(stdout, heap);

    debug("Аллоцируем очень большой блок, чтобы новый регион расширил старый\n");
    void* big_block = _malloc(MALLOC_SIZE_TEST2_1);
    if (!big_block) err("Не удалось аллоцировать большой блок");
    debug_heap(stdout, heap);

    debug("Освобождение большого блока\n");
    _free(big_block);
    debug_heap(stdout, heap);

    debug("-------------------\n");
}