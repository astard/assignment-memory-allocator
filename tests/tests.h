#ifndef _TESTS
#define _TESTS

#define _DEFAULT_SOURCE

#include "../src/mem.h"
#include "../src/mem_internals.h"
#include "../src/util.h"
#include <stdio.h>

#define HEAP_SIZE 10500

void debug (const char *fmt, ...);

void test1(void* heap);
void test2(void* heap);
void test3(void* heap);

#endif
