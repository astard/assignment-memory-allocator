#include "tests.h"

#define MALLOC_SIZE_TEST3_1 40000
#define MALLOC_EXISTED_SIZE_TEST3_2 4500

static struct block_header* find_last_block(struct block_header* start){
    struct block_header* last_block = start;
    while(last_block->next != NULL) last_block = last_block->next;
    return last_block;
}

void test3(void* heap){
    debug("Запущен тест 3 - инициирование кучи\n");
    
    debug_heap(stdout, heap);

    debug("Создаем тестовый блок, чтобы новый регион выделялся в другом месте\n");
    
    struct block_header* last_block = find_last_block((struct block_header*)heap);
    void* addr = (uint8_t*)last_block + size_from_capacity(last_block->capacity).bytes;
    map_pages(addr, MALLOC_EXISTED_SIZE_TEST3_2, MAP_FIXED);
    
    debug("Аллоцируем новый блок\n");
    void* new_block = _malloc(MALLOC_SIZE_TEST3_1);
    debug_heap(stdout, heap);

    if(block_get_header(new_block) == last_block) err("Неправильное выделение");

    debug("Освобождение нового блока\n");
    _free(new_block);
    debug_heap(stdout, heap);

    debug("-------------------\n");
}
