#include "../tests/tests.h"
#include "mem.h"
#include <stdio.h>

static void* init_heap(){
    void* heap = heap_init(HEAP_SIZE);
    if (heap == NULL) printf("Куча не создана\n");
    return heap;
}

int main(){
    void* heap = init_heap();
    test1(heap);
    test2(heap);
    test3(heap);
    return 0;
}